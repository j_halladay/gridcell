class Grid {

    constructor(options) {
        this.rowLength = options.rowLength
        this.cellLength = options.cellLength
        this.cellClass = options.cellClass
        this.rowClass = options.rowClass
        this.cellCoorodinatesArray = []
        this.cellWidth = options.cellWidth
        this.cellHeight = options.cellHeight
        this.createGrid()
    }
    createGrid() {

        
        for (let rowIndex = 0; rowIndex < this.rowLength; rowIndex++) {
            let rowElement = document.createElement("div")
            rowElement.classList.add("row")
            for (let cellIndex = 0; cellIndex < this.cellLength; cellIndex++) {
                console.log(cellIndex)
                new Cell(this.cellWidth, this.cellHeight, rowIndex, cellIndex, rowElement)
                this.cellCoorodinatesArray[rowIndex] = cellIndex

            }
            document.body.appendChild(rowElement)
            
        }
        
    }
    cellSearch(row,column){
        
        let cellSearchResult=document.querySelector(`[data-row-column="${row},${column}"]`)
        return cellSearchResult
        

    }
    cellNeighbors(cell){
        let targetCellData=cell.dataset.rowColumn
        let cellArray= targetCellData.split(",")
        let row=Number(cellArray[0])
        let column=Number(cellArray[1])
        let topCell=document.querySelector(`[data-row-column="${row-1},${column}"]`)
        let bottomCell=document.querySelector(`[data-row-column="${row+1},${column}"]`)
        let leftCell=document.querySelector(`[data-row-column="${row},${column-1}"]`)
        let rightCell=document.querySelector(`[data-row-column="${row},${column+1}"]`)
        if (topCell===null){

        }else{
            topCell.style.backgroundColor="green"
        }
        if (bottomCell===null){

        }else{
            bottomCell.style.backgroundColor="green"
        }
        if (leftCell===null){

        }else{
            leftCell.style.backgroundColor="green"
        }
        if (rightCell===null){

        }else{
            rightCell.style.backgroundColor="green"
        }
    }
    
}